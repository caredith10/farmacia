/*
 Diseñe la estructura de la aplicación siguiendo un modelo por capas.
Crea la vista o capa de presentación con componentes gráficos.
Conecte la base de datos e implemente los métodos necesarios para las funcionalidades del programa.
PARTE 2:
De acuerdo al desarrollo de una aplicación por capas, cree un software que muestre una tabla con los datos de los Productos presentes dentro de una base de datos SQLite. 
El software debe permitir crear, actualizar, eliminar o consultar sobre la tabla de productos de la base de datos.
De forma OPCIONAL, puede crear una tabla adicional llamada Farmacias, y hacer una pestaña específica con las mismas funcionalidades CRUD que en la pestaña Productos.

// Aplicación: TRABAJO_RETO5
// Aplicación que permite guradar, actualizar, eliminar registros de la tabla, consultar todos los productos existentes en la base de datos y borrar los campos de las cajas de texto
/ Se deben ingresar todos los campos
 */
package logica;

/**
 *
 * @author Carmen
 */
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import persistencia.ConexionBD;

/**
 *
 * @author Carmen
 */

// Declaración de variables
public  class productos {

    private int id;
    private String nombre;
    private int valorBase;
    private int temperatura;
    private double costo;
    
    
   //Metodo constructor vacío
    public productos() {
    }
    
  // Metodo constructor  con argumentos
    public productos(String nombre, int valorBase, int temperatura, double costo) {
        this.nombre = nombre;
        this.valorBase = valorBase;
        this.temperatura = temperatura;
        this.costo = costo;
    }

    // Metodos getter y setter
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getValorBase() {
        return valorBase;
    }

    public void setValorBase(int valorBase) {
        this.valorBase = valorBase;
    }

    public int getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(int temperatura) {
        this.temperatura = temperatura;
    }

    public double getCosto() {
        return costo;
    }

    public void setCosto(double costo) {
        this.costo = costo;
    }

//    // public abstract double calcularCostoDeAlmacenamiento();
//     public double calcularCostoDeAlmacenamiento(){
//         
//     return 10;   
//    }
    
    // Sobreescritura de métodos 
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Producto{id=").append(id);
        sb.append(", nombre=").append(nombre);
        sb.append(", valorBase=").append(valorBase);
        sb.append(", temperatura=").append(temperatura);
        sb.append(", costo=").append(costo);
        sb.append('}');
        return sb.toString();
    }
    
    
  //  Metodo para Calcular el costo de almacenamiento segun la temperatura del producto
//    public double calcularCosto(){
//        double costo=0;
//        if(this.getTemperatura()>=0&&this.getTemperatura()<= 20){
//            costo = this.getValorBase()*1.20;
//        }
//        else if(this.getTemperatura()>20){
//            costo=this.getValorBase()*1.10;
//        }
//        return costo;
//    }     
            
       
    
    
    
    
    //
     public productos getProducto(int id) {
        ConexionBD conexion = new ConexionBD();
        String sql = "select * from productos WHERE id="+id+";";
        try {
            ResultSet rs = conexion.consultarBD(sql);
            if (rs.next()) {
                this.id = rs.getInt("id");
                this.valorBase = rs.getInt("valorBase");
                this.nombre = rs.getString("nombre");
                this.temperatura = rs.getInt("temperatura");
                this.costo = rs.getDouble("costo");
            }
        } catch (SQLException ex) {
            System.out.println("Error" + ex.getMessage());
        } finally {
            conexion.cerrarConexion();
        }
        return this;
    }

   // Metodo pra generar el listado de los productos que se encuentran en la base de datos ingresados por pantalla   
    public List<productos> listarProductos() {
        List<productos> listaProductos = new ArrayList<>();
        ConexionBD conexion = new ConexionBD();
        String sql = "SELECT * FROM productos;";
        try {
            ResultSet rs = conexion.consultarBD(sql);
            productos p;
            while (rs.next()) {
                p = new productos();
                p.setId(rs.getInt("id"));
                p.setNombre(rs.getString("nombre"));
                p.setTemperatura(rs.getInt("temperatura"));
                p.setValorBase(rs.getInt("valorBase"));
                p.setCosto(rs.getDouble("costo"));
                listaProductos.add(p);
            }
        } catch (SQLException ex) {
            System.out.println("Error al listar productos:" + ex.getMessage());
        } finally {
            conexion.cerrarConexion();
        }
        return listaProductos;
    }
    
    
    
    //Metodo para guardar o registrar a la base de datos  sqlite los productos ingresados por pantalla
    
    public boolean guardarProducto() {
        ConexionBD conexion = new ConexionBD();
        String sql = "INSERT INTO productos(nombre,temperatura,valorBase,costo)"
                + "VALUES('" + this.nombre + "'," + this.valorBase+ ",'" + this.temperatura + "'," + this.costo+ ");";
        if (conexion.setAutoCommitBD(false)) {//Para que la bd no confirme automaticamente el cambio
            if (conexion.insertarBD(sql)) {
                conexion.commitBD();//confirma el cambio a la BD
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }

   
    
    
    //Metodo para actualizar los productos, ingresando los datos del producto a cambiar
    
     public boolean actualizarProducto() {
        ConexionBD conexion = new ConexionBD();
        String sql = "UPDATE productos SET nombre='"
                + this.nombre + "',temperatura=" + this.temperatura
                + ",valorBase='" + this.valorBase + "',costo="
                + this.costo + " WHERE id=" + this.id + ";";
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.actualizarBD(sql)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }
     
     
     
     //Metodo pra eliminar los registros de la tabla
     public boolean eliminarProducto() {
        ConexionBD conexion = new ConexionBD();
        String sql = "DELETE FROM productos WHERE id=" + this.id + ";";
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.actualizarBD(sql)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }
}


